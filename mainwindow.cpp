#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QInputDialog>
#include <QFile>
#include <QMouseEvent>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),
      time(0,0,0,0)
{
    ui->setupUi(this);

    setStyleSheet("MainWindow { background-color: #565656 } QPushButton {background-color: #565656 }");
    QPalette sample_palette;
    sample_palette.setColor(QPalette::Window, QColor(0x56, 0x56, 0x56));
    sample_palette.setColor(QPalette::WindowText, QColor(0xa9, 0xa9, 0xa9));
    sample_palette.setColor(QPalette::Foreground, QColor(0xa9, 0xa9, 0xa9));
    setPalette(sample_palette);
    ui->timeLabel->setPalette(sample_palette);

//    QFile sf(":/style/darkStyle.qss");
//    sf.open(QIODevice::ReadOnly);
//    setStyleSheet(sf.readAll());
//    sf.close();
    ui->mainLayout->setAlignment(ui->timeLabel, Qt::AlignCenter);
    ui->timeLabel->setText(time.toString("hh:mm:ss"));
    auto tl = ui->timeLabel;
    connect(&timer, &QTimer::timeout,[&](){
        time = time.addSecs(1);
        ui->timeLabel->setText(time.toString("hh:mm:ss"));
    });

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_startButton_clicked()
{
    if(timer.isActive())
        return;
    timer.start(1000);
}

void MainWindow::on_stopButton_clicked()
{
    if(!timer.isActive())
        return;
    timer.stop();
}

void MainWindow::on_initButton_clicked()
{
    bool ok = false;
    auto rez = QInputDialog::getText(this, "", "hh:mm:ss",QLineEdit::Normal, "00:00:00", &ok);
    if(ok)
    {
        time = QTime::fromString(rez, "hh:mm:ss");
        ui->timeLabel->setText(time.toString("hh:mm:ss"));
    }
}

void MainWindow::mousePressEvent(QMouseEvent* event)
{
    m_isDragged = true;
    oldPos = event->globalPos() - pos();
}

void MainWindow::mouseReleaseEvent(QMouseEvent* event)
{
    m_isDragged = false;
}

void MainWindow::mouseMoveEvent(QMouseEvent* event)
{
    if(m_isDragged)
    {
        move(event->globalPos() - oldPos);
    }
}
